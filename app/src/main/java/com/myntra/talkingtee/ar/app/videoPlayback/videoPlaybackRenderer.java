/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.myntra.talkingtee.ar.app.videoPlayback;

import java.math.BigInteger;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;

import com.vuforia.CameraCalibration;
import com.vuforia.CameraDevice;
import com.vuforia.Device;
import com.vuforia.Image;
import com.vuforia.InstanceId;
import com.vuforia.Matrix44F;
import com.vuforia.PIXEL_FORMAT;
import com.vuforia.Renderer;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Vec2F;
import com.vuforia.Vec3F;
import com.vuforia.VuMarkTarget;
import com.vuforia.VuMarkTargetResult;
import com.vuforia.VuMarkTemplate;
import com.vuforia.Vuforia;
import com.myntra.talkingtee.talkingteeApp.talkingteeAppSession;
import com.myntra.talkingtee.talkingteeApp.talkingteeAppRenderer;
import com.myntra.talkingtee.talkingteeApp.talkingteeAppRendererControl;
import com.myntra.talkingtee.talkingteeApp.utils.CubeShaders;
import com.myntra.talkingtee.talkingteeApp.utils.SampleMath;
import com.myntra.talkingtee.talkingteeApp.utils.SampleUtils;
import com.myntra.talkingtee.talkingteeApp.utils.Texture;
import com.myntra.talkingtee.ar.app.videoPlayback.videoPlayerHelper.MEDIA_STATE;
import com.myntra.talkingtee.ar.app.videoPlayback.videoPlayerHelper.MEDIA_TYPE;


// The renderer class for the videoPlayback sample.
public class videoPlaybackRenderer implements GLSurfaceView.Renderer, talkingteeAppRendererControl
{
    private static final String LOGTAG = "videoPlaybackRenderer";

    talkingteeAppSession vuforiaAppSession;
    talkingteeAppRenderer mtalkingteeAppRenderer;

    private double t0;

    // ratio to apply so that the augmentation surrounds the vumark
    private static final float VUMARK_SCALE = 1.02f;
    private String currentVumarkIdOnCard;


    // Video Playback Rendering Specific
    private int videoPlaybackShaderID = 0;
    private int videoPlaybackVertexHandle = 0;
    private int videoPlaybackTexCoordHandle = 0;
    private int videoPlaybackMVPMatrixHandle = 0;
    private int videoPlaybackTexSamplerOESHandle = 0;

    // Video Playback Textures for the two targets
    int videoPlaybackTextureID[] = new int[videoPlayback.NUM_TARGETS];

    // Keyframe and icon rendering specific
    private int keyframeShaderID = 0;
    private int keyframeVertexHandle = 0;
    private int keyframeTexCoordHandle = 0;
    private int keyframeMVPMatrixHandle = 0;
    private int keyframeTexSampler2DHandle = 0;

    // We cannot use the default texture coordinates of the quad since these
    // will change depending on the video itself
    private float videoQuadTextureCoords[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 1.0f, };

    // This variable will hold the transformed coordinates (changes every frame)
    private float videoQuadTextureCoordsTransformedVideo1[] = { 0.0f, 0.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, };

    private float videoQuadTextureCoordsTransformedVideo2[] = { 0.0f, 0.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, };

    private float videoQuadTextureCoordsTransformedVideo3[] = { 0.0f, 0.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, };

    // Trackable dimensions
    Vec3F targetPositiveDimensions[] = new Vec3F[videoPlayback.NUM_TARGETS];

    static int NUM_QUAD_VERTEX = 4;
    static int NUM_QUAD_INDEX = 6;

    double quadVerticesArray[] = { -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f,
            1.0f, 0.0f, -1.0f, 1.0f, 0.0f };

    double quadTexCoordsArray[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            1.0f };

    double quadNormalsArray[] = { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, };

    short quadIndicesArray[] = { 0, 1, 2, 2, 3, 0 };

    Buffer quadVertices, quadTexCoords, quadIndices, quadNormals;

    private boolean mIsActive = false;
    private Matrix44F tappingProjectionMatrix = null;

    private float[][] mTexCoordTransformationMatrix = null;
    private videoPlayerHelper mVideoPlayerHelper[] = null;
    private String mMovieName[] = null;
    private MEDIA_TYPE mCanRequestType[] = null;
    private int mSeekPosition[] = null;
    private boolean mShouldPlayImmediately[] = null;
    private long mLostTrackingSince[] = null;
    private boolean mLoadRequested[] = null;

    videoPlayback mActivity;

    // Needed to calculate whether a screen tap is inside the target
    Matrix44F modelViewMatrix[] = new Matrix44F[videoPlayback.NUM_TARGETS];

    private Vector<Texture> mTextures;

    boolean isTracking[] = new boolean[videoPlayback.NUM_TARGETS];
    MEDIA_STATE currentStatus[] = new MEDIA_STATE[videoPlayback.NUM_TARGETS];

    // These hold the aspect ratio of both the video and the
    // keyframe
    float videoQuadAspectRatio[] = new float[videoPlayback.NUM_TARGETS];
    float keyframeQuadAspectRatio[] = new float[videoPlayback.NUM_TARGETS];


    private int shaderProgramID;
    private int vertexHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;
    private int texSampler2DHandle;
    private int calphaHandle;

    private Renderer mRenderer;

    private final Plane mPlaneObj;

    public videoPlaybackRenderer(videoPlayback activity,
                                 talkingteeAppSession session)
    {

        mActivity = activity;
        vuforiaAppSession = session;

        t0 = -1.0;
        mPlaneObj = new Plane();

        // talkingteeAppRenderer used to encapsulate the use of RenderingPrimitives setting
        // the device mode AR/VR and stereo mode
        mtalkingteeAppRenderer = new talkingteeAppRenderer(this, mActivity, Device.MODE.MODE_AR, false, 0.01f, 100f);

        // Create an array of the size of the number of targets we have
        mVideoPlayerHelper = new videoPlayerHelper[videoPlayback.NUM_TARGETS];
        mMovieName = new String[videoPlayback.NUM_TARGETS];
        mCanRequestType = new MEDIA_TYPE[videoPlayback.NUM_TARGETS];
        mSeekPosition = new int[videoPlayback.NUM_TARGETS];
        mShouldPlayImmediately = new boolean[videoPlayback.NUM_TARGETS];
        mLostTrackingSince = new long[videoPlayback.NUM_TARGETS];
        mLoadRequested = new boolean[videoPlayback.NUM_TARGETS];
        mTexCoordTransformationMatrix = new float[videoPlayback.NUM_TARGETS][16];

        // Initialize the arrays to default values
        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
        {
            mVideoPlayerHelper[i] = null;
            mMovieName[i] = "";
            mCanRequestType[i] = MEDIA_TYPE.ON_TEXTURE_FULLSCREEN;
            mSeekPosition[i] = 0;
            mShouldPlayImmediately[i] = false;
            mLostTrackingSince[i] = -1;
            mLoadRequested[i] = false;
        }

        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
            targetPositiveDimensions[i] = new Vec3F();

        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
            modelViewMatrix[i] = new Matrix44F();
    }


    // Store the Player Helper object passed from the main activity
    public void setVideoPlayerHelper(int target,
                                     videoPlayerHelper newVideoPlayerHelper)
    {
        mVideoPlayerHelper[target] = newVideoPlayerHelper;
    }


    public void requestLoad(int target, String movieName, int seekPosition,
                            boolean playImmediately)
    {
        mMovieName[target] = movieName;
        mSeekPosition[target] = seekPosition;
        mShouldPlayImmediately[target] = playImmediately;
        mLoadRequested[target] = true;
    }


    // Called when the surface is created or recreated.
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // Call function to initialize rendering:
        // The video texture is also created on this step
        initRendering();

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        Vuforia.onSurfaceCreated();

        mtalkingteeAppRenderer.onSurfaceCreated();

        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
        {

            if (mVideoPlayerHelper[i] != null)
            {
                // The videoPlayerHelper needs to setup a surface texture given
                // the texture id
                // Here we inform the video player that we would like to play
                // the movie
                // both on texture and on full screen
                // Notice that this does not mean that the platform will be able
                // to do what we request
                // After the file has been loaded one must always check with
                // isPlayableOnTexture() whether
                // this can be played embedded in the AR scene
                if (!mVideoPlayerHelper[i]
                        .setupSurfaceTexture(videoPlaybackTextureID[i]))
                    mCanRequestType[i] = MEDIA_TYPE.FULLSCREEN;
                else
                    mCanRequestType[i] = MEDIA_TYPE.ON_TEXTURE_FULLSCREEN;

                // And now check if a load has been requested with the
                // parameters passed from the main activity
                if (mLoadRequested[i])
                {
                    mVideoPlayerHelper[i].load(mMovieName[i],
                            mCanRequestType[i], mShouldPlayImmediately[i],
                            mSeekPosition[i]);
                    mLoadRequested[i] = false;
                }
            }
        }
    }


    // Called when the surface changed size.
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        // Call Vuforia function to handle render surface size changes:
        Vuforia.onSurfaceChanged(width, height);

        // RenderingPrimitives to be updated when some rendering change is done
        mtalkingteeAppRenderer.onConfigurationChanged(mIsActive);

        // Upon every on pause the movie had to be unloaded to release resources
        // Thus, upon every surface create or surface change this has to be
        // reloaded
        // See:
        // http://developer.android.com/reference/android/media/MediaPlayer.html#release()
        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
        {
            if (mLoadRequested[i] && mVideoPlayerHelper[i] != null)
            {
                mVideoPlayerHelper[i].load(mMovieName[i], mCanRequestType[i],
                        mShouldPlayImmediately[i], mSeekPosition[i]);
                mLoadRequested[i] = false;
            }
        }
    }


    // Called to draw the current frame.
    public void onDrawFrame(GL10 gl)
    {
        if (!mIsActive)
            return;

        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
        {
            if (mVideoPlayerHelper[i] != null)
            {
                if (mVideoPlayerHelper[i].isPlayableOnTexture())
                {
                    // First we need to update the video data. This is a built
                    // in Android call
                    // Here, the decoded data is uploaded to the OES texture
                    // We only need to do this if the movie is playing
                    if (mVideoPlayerHelper[i].getStatus() == MEDIA_STATE.PLAYING)
                    {
                        mVideoPlayerHelper[i].updateVideoData();
                    }

                    // According to the Android API
                    // (http://developer.android.com/reference/android/graphics/SurfaceTexture.html)
                    // transforming the texture coordinates needs to happen
                    // every frame.
                    mVideoPlayerHelper[i]
                            .getSurfaceTextureTransformMatrix(mTexCoordTransformationMatrix[i]);
                    setVideoDimensions(i,
                            mVideoPlayerHelper[i].getVideoWidth(),
                            mVideoPlayerHelper[i].getVideoHeight(),
                            mTexCoordTransformationMatrix[i]);
                }

                setStatus(i, mVideoPlayerHelper[i].getStatus().getNumericType());
            }
        }

        // Call our function to render content from talkingteeAppRenderer class
        mtalkingteeAppRenderer.render();

        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
        {
            // Ask whether the target is currently being tracked and if so react
            // to it
            if (isTracking(i))
            {
                // If it is tracking reset the timestamp for lost tracking
                mLostTrackingSince[i] = -1;
            } else
            {
                // If it isn't tracking
                // check whether it just lost it or if it's been a while
                if (mLostTrackingSince[i] < 0)
                    mLostTrackingSince[i] = SystemClock.uptimeMillis();
                else
                {
                    // If it's been more than 2 seconds then pause the player
                    if ((SystemClock.uptimeMillis() - mLostTrackingSince[i]) > 2000)
                    {
                        if (mVideoPlayerHelper[i] != null)
                            mVideoPlayerHelper[i].pause();
                    }
                }
            }
        }

        // If you would like the video to start playing as soon as it starts
        // tracking
        // and pause as soon as tracking is lost you can do that here by
        // commenting
        // the for-loop above and instead checking whether the isTracking()
        // value has
        // changed since the last frame. Notice that you need to be careful not
        // to
        // trigger automatic playback for fullscreen since that will be
        // inconvenient
        // for your users.

    }


    public void setActive(boolean active)
    {
        mIsActive = active;

        if(mIsActive)
            mtalkingteeAppRenderer.configureVideoBackground();
    }


    @SuppressLint("InlinedApi")
    void initRendering()
    {
        Log.d(LOGTAG, "videoPlayback videoPlaybackRenderer initRendering");

        // Define clear color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
                : 1.0f);

        // Now generate the OpenGL texture objects and add settings
        for (Texture t : mTextures)
        {
            // Here we create the textures for the keyframe
            // and for all the icons
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                    t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
                    GLES20.GL_UNSIGNED_BYTE, t.mData);
        }

        // Now we create the texture for the video data from the movie
        // IMPORTANT:
        // Notice that the textures are not typical GL_TEXTURE_2D textures
        // but instead are GL_TEXTURE_EXTERNAL_OES extension textures
        // This is required by the Android SurfaceTexture
        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++)
        {
            GLES20.glGenTextures(1, videoPlaybackTextureID, i);
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                    videoPlaybackTextureID[i]);
            GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);
        }

        // The first shader is the one that will display the video data of the
        // movie
        // (it is aware of the GL_TEXTURE_EXTERNAL_OES extension)
        videoPlaybackShaderID = SampleUtils.createProgramFromShaderSrc(
                videoPlaybackShaders.VIDEO_PLAYBACK_VERTEX_SHADER,
                videoPlaybackShaders.VIDEO_PLAYBACK_FRAGMENT_SHADER);
        videoPlaybackVertexHandle = GLES20.glGetAttribLocation(
                videoPlaybackShaderID, "vertexPosition");
        videoPlaybackTexCoordHandle = GLES20.glGetAttribLocation(
                videoPlaybackShaderID, "vertexTexCoord");
        videoPlaybackMVPMatrixHandle = GLES20.glGetUniformLocation(
                videoPlaybackShaderID, "modelViewProjectionMatrix");
        videoPlaybackTexSamplerOESHandle = GLES20.glGetUniformLocation(
                videoPlaybackShaderID, "texSamplerOES");

        // This is a simpler shader with regular 2D textures
        keyframeShaderID = SampleUtils.createProgramFromShaderSrc(
                KeyFrameShaders.KEY_FRAME_VERTEX_SHADER,
                KeyFrameShaders.KEY_FRAME_FRAGMENT_SHADER);
        keyframeVertexHandle = GLES20.glGetAttribLocation(keyframeShaderID,
                "vertexPosition");
        keyframeTexCoordHandle = GLES20.glGetAttribLocation(keyframeShaderID,
                "vertexTexCoord");
        keyframeMVPMatrixHandle = GLES20.glGetUniformLocation(keyframeShaderID,
                "modelViewProjectionMatrix");
        keyframeTexSampler2DHandle = GLES20.glGetUniformLocation(
                keyframeShaderID, "texSampler2D");

        keyframeQuadAspectRatio[videoPlayback.VIDEO1] = (float) mTextures
                .get(0).mHeight / (float) mTextures.get(0).mWidth;
        keyframeQuadAspectRatio[videoPlayback.VIDEO2] = (float) mTextures.get(1).mHeight
                / (float) mTextures.get(1).mWidth;

        quadVertices = fillBuffer(quadVerticesArray);
        quadTexCoords = fillBuffer(quadTexCoordsArray);
        quadIndices = fillBuffer(quadIndicesArray);
        quadNormals = fillBuffer(quadNormalsArray);

        mRenderer = Renderer.getInstance();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
                : 1.0f);

        for (Texture t : mTextures)
        {
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA,
                    t.mWidth, t.mHeight, 0, GLES20.GL_RGBA,
                    GLES20.GL_UNSIGNED_BYTE, t.mData);
        }

        shaderProgramID = SampleUtils.createProgramFromShaderSrc(
                CubeShaders.CUBE_MESH_VERTEX_SHADER,
                CubeShaders.CUBE_MESH_FRAGMENT_SHADER);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexPosition");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID,
                "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "modelViewProjectionMatrix");
        texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "texSampler2D");
        calphaHandle = GLES20.glGetUniformLocation(shaderProgramID,
                "calpha");


    }


    private Buffer fillBuffer(double[] array)
    {
        // Convert to floats because OpenGL doesnt work on doubles, and manually
        // casting each input value would take too much time.
        ByteBuffer bb = ByteBuffer.allocateDirect(4 * array.length); // each float takes 4 bytes
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (double d : array)
            bb.putFloat((float) d);
        bb.rewind();

        return bb;

    }


    private Buffer fillBuffer(short[] array)
    {
        ByteBuffer bb = ByteBuffer.allocateDirect(2 * array.length); // each
        // short
        // takes 2
        // bytes
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (short s : array)
            bb.putShort(s);
        bb.rewind();

        return bb;

    }


    private Buffer fillBuffer(float[] array)
    {
        // Convert to floats because OpenGL doesnt work on doubles, and manually
        // casting each input value would take too much time.
        ByteBuffer bb = ByteBuffer.allocateDirect(4 * array.length); // each float takes 4 bytes
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (float d : array)
            bb.putFloat(d);
        bb.rewind();

        return bb;

    }

    // The render function called from talkingteeAppRendering by using RenderingPrimitives views.
    // The state is owned by talkingteeAppRenderer which is controlling it's lifecycle.
    // State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix) {
        // Renders video background replacing Renderer.DrawVideoBackground()
        mtalkingteeAppRenderer.renderVideoBackground();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        boolean gotVuMark = false;

        String markerType = "";
        String markerValue = "";
        Bitmap markerBitmap = null;
        int indexVuMarkToDisplay = -1;

        if (tappingProjectionMatrix == null) {
            tappingProjectionMatrix = new Matrix44F();
            tappingProjectionMatrix.setData(projectionMatrix);
        }

        float temp[] = {0.0f, 0.0f, 0.0f};
        for (int i = 0; i < videoPlayback.NUM_TARGETS; i++) {
            isTracking[i] = false;
            targetPositiveDimensions[i].setData(temp);
        }

        if (state.getNumTrackableResults() > 1) {
            float minimumDistance = Float.MAX_VALUE;
            CameraCalibration cameraCalibration = CameraDevice.getInstance().getCameraCalibration();
            Vec2F screenSize = cameraCalibration.getSize();
            Vec2F screenCenter = new Vec2F(screenSize.getData()[0] / 2.0f, screenSize.getData()[1] / 2.0f);

            for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
                TrackableResult result = state.getTrackableResult(tIdx);
                if (result.isOfType(VuMarkTargetResult.getClassType())) {
                    Vec3F point = new Vec3F(0, 0, 0);
                    Vec2F projection = Tool.projectPoint(cameraCalibration, result.getPose(), point);

                    float distance = distanceSquared(projection, screenCenter);
                    if (distance < minimumDistance) {
                        minimumDistance = distance;
                        indexVuMarkToDisplay = tIdx;
                    }
                }
            }

        }

        // Did we find any trackables this frame?
        for (int tIdx = 0; tIdx < state.getNumTrackableResults(); tIdx++) {
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();
            Matrix44F modelViewMatrix_Vuforia = Tool
                    .convertPose2GLMatrix(result.getPose());
            float[] modelViewMatrix1 = modelViewMatrix_Vuforia.getData();

            if (result.isOfType(VuMarkTargetResult.getClassType())) {
                VuMarkTargetResult vmtResult = (VuMarkTargetResult) result;
                VuMarkTarget vmTgt = (VuMarkTarget) vmtResult.getTrackable();

                VuMarkTemplate vmTmp = (VuMarkTemplate) vmTgt.getTemplate();
                // user data contains the the SVG layer corresponding to the contour
                // of the VuMark template
                // look at the iOS sample app to see how this data can be used to dynamically
                // render an OpenGL object on top of the contour.
                String userData = vmTmp.getVuMarkUserData();

                InstanceId instanceId = vmTgt.getInstanceId();
                boolean isMainVuMark = ((indexVuMarkToDisplay < 0) ||
                        (indexVuMarkToDisplay == tIdx));
                gotVuMark = true;

                if (isMainVuMark) {
                    markerValue = instanceIdToValue(instanceId).toString().trim();
                    markerType = instanceIdToType(instanceId);
                    Image instanceImage = vmTgt.getInstanceImage();
                    markerBitmap = getBitMapFromImage(instanceImage);

                    if (!markerValue.equalsIgnoreCase(currentVumarkIdOnCard)) {
                        //mActivity.hideCard();
                        blinkVumark(true);
                    }
                }
                int textureIndex = 0;

                // deal with the modelview and projection matrices
                float[] modelViewProjection = new float[16];

                // Add a translation to recenter the augmentation
                // on the VuMark center, w.r.t. the origin
                Vec2F origin = vmTmp.getOrigin();
                float translX = -origin.getData()[0];
                float translY = -origin.getData()[1];
                Matrix.translateM(modelViewMatrix1, 0, translX, translY, 0);

                // Scales the plane relative to the target
                float vumarkWidth = vmTgt.getSize().getData()[0];
                float vumarkHeight = vmTgt.getSize().getData()[1];
                Matrix.scaleM(modelViewMatrix1, 0, vumarkWidth * VUMARK_SCALE,
                        vumarkHeight * VUMARK_SCALE, 1.0f);

                Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrix1, 0);
/*
                // activate the shader program and bind the vertex/normal/tex coords
                GLES20.glUseProgram(shaderProgramID);

                GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT,
                        false, 0, mPlaneObj.getVertices());
                GLES20.glVertexAttribPointer(textureCoordHandle, 2,
                        GLES20.GL_FLOAT, false, 0, mPlaneObj.getTexCoords());

                GLES20.glEnableVertexAttribArray(vertexHandle);
                GLES20.glEnableVertexAttribArray(textureCoordHandle);

                // activate texture 0, bind it, and pass to shader
                GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                        mTextures.get(textureIndex).mTextureID[0]);
                GLES20.glUniform1i(texSampler2DHandle, 0);
                GLES20.glUniform1f(calphaHandle, isMainVuMark ? blinkVumark(false) : 1.0f);

                // pass the model view matrix to the shader
                GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false,
                        modelViewProjection, 0);

                // finally draw the plane
                GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                        mPlaneObj.getNumObjectIndex(), GLES20.GL_UNSIGNED_SHORT,
                        mPlaneObj.getIndices());*/
/*
                // disable the enabled arrays
                GLES20.glDisableVertexAttribArray(vertexHandle);
                GLES20.glDisableVertexAttribArray(textureCoordHandle);
                SampleUtils.checkGLError("Render Frame");
*/

                int currentTarget = -1;

                if (markerValue.toLowerCase().compareTo("0001") == 0)
                    currentTarget = videoPlayback.VIDEO1;
                else
                if (markerValue.toLowerCase().compareTo("0002") == 0)
                    currentTarget = videoPlayback.VIDEO2;
                else
                if (markerValue.toLowerCase().compareTo("0003") == 0)
                    currentTarget = videoPlayback.VIDEO3;

                if (gotVuMark && currentTarget>-1)
                {

                    // If we have a detection, let's make sure
                    // the card is visible
                    //mActivity.showCard(markerType, markerValue, markerBitmap);
                    //mActivity.showToast(markerValue);
                    currentVumarkIdOnCard = markerValue;

                    modelViewMatrix[currentTarget] = Tool
                            .convertPose2GLMatrix(result.getPose());

                    isTracking[currentTarget] = true;

                    targetPositiveDimensions[currentTarget] = vmTgt.getSize();//imageTarget.getSize();

                    // The pose delivers the center of the target, thus the dimensions
                    // go from -width/2 to width/2, same for height
                    temp[0] = targetPositiveDimensions[currentTarget].getData()[0] / 2f;
                    temp[1] = targetPositiveDimensions[currentTarget].getData()[1] / 2f;
                    targetPositiveDimensions[currentTarget].setData(temp);


                    // If the movie is ready to start playing or it has reached the end
                    // of playback we render the keyframe
                    if ((currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.READY)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.REACHED_END)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.NOT_READY)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.ERROR)) {
                        float[] modelViewMatrixKeyframe = Tool.convertPose2GLMatrix(
                                result.getPose()).getData();
                        float[] modelViewProjectionKeyframe = new float[16];
                        // Matrix.translateM(modelViewMatrixKeyframe, 0, 0.0f, 0.0f,
                        // targetPositiveDimensions[currentTarget].getData()[0]);

                        // Here we use the aspect ratio of the keyframe since it
                        // is likely that it is not a perfect square

                        float ratio = 1f;
                        if (mTextures.get(currentTarget).mSuccess)
                            ratio = keyframeQuadAspectRatio[currentTarget];
                        else
                            ratio = targetPositiveDimensions[currentTarget].getData()[1]
                                    / targetPositiveDimensions[currentTarget].getData()[0];

                        Matrix.scaleM(modelViewMatrixKeyframe, 0,
                                targetPositiveDimensions[currentTarget].getData()[0],
                                targetPositiveDimensions[currentTarget].getData()[0]
                                        * ratio,
                                targetPositiveDimensions[currentTarget].getData()[0]);
                        Matrix.multiplyMM(modelViewProjectionKeyframe, 0,
                                projectionMatrix, 0, modelViewMatrixKeyframe, 0);

                        GLES20.glUseProgram(keyframeShaderID);

                        // Prepare for rendering the keyframe
                        GLES20.glVertexAttribPointer(keyframeVertexHandle, 3,
                                GLES20.GL_FLOAT, false, 0, quadVertices);
                        GLES20.glVertexAttribPointer(keyframeTexCoordHandle, 2,
                                GLES20.GL_FLOAT, false, 0, quadTexCoords);

                        GLES20.glEnableVertexAttribArray(keyframeVertexHandle);
                        GLES20.glEnableVertexAttribArray(keyframeTexCoordHandle);

                        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

                        // The first loaded texture from the assets folder is the
                        // keyframe
                        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                mTextures.get(currentTarget).mTextureID[0]);
                        GLES20.glUniformMatrix4fv(keyframeMVPMatrixHandle, 1, false,
                                modelViewProjectionKeyframe, 0);
                        GLES20.glUniform1i(keyframeTexSampler2DHandle, 0);

                        // Render
                        GLES20.glDrawElements(GLES20.GL_TRIANGLES, NUM_QUAD_INDEX,
                                GLES20.GL_UNSIGNED_SHORT, quadIndices);

                        GLES20.glDisableVertexAttribArray(keyframeVertexHandle);
                        GLES20.glDisableVertexAttribArray(keyframeTexCoordHandle);

                        GLES20.glUseProgram(0);
                    } else
                    // In any other case, such as playing or paused, we render
                    // the actual contents
                    {
                        float[] modelViewMatrixVideo = Tool.convertPose2GLMatrix(
                                result.getPose()).getData();
                        float[] modelViewProjectionVideo = new float[16];
                        // Matrix.translateM(modelViewMatrixVideo, 0, 0.0f, 0.0f,
                        // targetPositiveDimensions[currentTarget].getData()[0]);

                        // Here we use the aspect ratio of the video frame
                        Matrix.scaleM(modelViewMatrixVideo, 0,
                                targetPositiveDimensions[currentTarget].getData()[0],
                                targetPositiveDimensions[currentTarget].getData()[0]
                                        * videoQuadAspectRatio[currentTarget],
                                targetPositiveDimensions[currentTarget].getData()[0]);
                        Matrix.multiplyMM(modelViewProjectionVideo, 0,
                                projectionMatrix, 0, modelViewMatrixVideo, 0);

                        GLES20.glUseProgram(videoPlaybackShaderID);

                        // Prepare for rendering the keyframe
                        GLES20.glVertexAttribPointer(videoPlaybackVertexHandle, 3,
                                GLES20.GL_FLOAT, false, 0, quadVertices);

                        /* //if (imageTarget.getName().compareTo("RLogo") == 0)
                        GLES20.glVertexAttribPointer(videoPlaybackTexCoordHandle,
                                2, GLES20.GL_FLOAT, false, 0,
                                fillBuffer(videoQuadTextureCoordsTransformedVideo1));
                            else
                        */
                                GLES20.glVertexAttribPointer(videoPlaybackTexCoordHandle,
                                        2, GLES20.GL_FLOAT, false, 0,
                                        fillBuffer(videoQuadTextureCoordsTransformedVideo2));


                        GLES20.glEnableVertexAttribArray(videoPlaybackVertexHandle);
                        GLES20.glEnableVertexAttribArray(videoPlaybackTexCoordHandle);

                        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

                        // IMPORTANT:
                        // Notice here that the texture that we are binding is not the
                        // typical GL_TEXTURE_2D but instead the GL_TEXTURE_EXTERNAL_OES
                        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                                videoPlaybackTextureID[currentTarget]);
                        GLES20.glUniformMatrix4fv(videoPlaybackMVPMatrixHandle, 1,
                                false, modelViewProjectionVideo, 0);
                        GLES20.glUniform1i(videoPlaybackTexSamplerOESHandle, 0);

                        // Render
                        GLES20.glDrawElements(GLES20.GL_TRIANGLES, NUM_QUAD_INDEX,
                                GLES20.GL_UNSIGNED_SHORT, quadIndices);

                        GLES20.glDisableVertexAttribArray(videoPlaybackVertexHandle);
                        GLES20.glDisableVertexAttribArray(videoPlaybackTexCoordHandle);

                        GLES20.glUseProgram(0);

                    }


                    // The following section renders the icons. The actual textures used
                    // are loaded from the assets folder

                    if ((currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.READY)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.REACHED_END)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.PAUSED)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.NOT_READY)
                            || (currentStatus[currentTarget] == videoPlayerHelper.MEDIA_STATE.ERROR)) {


                        // If the movie is ready to be played, pause, has reached end or
                        // is not
                        // ready then we display one of the icons
                        float[] modelViewMatrixButton = Tool.convertPose2GLMatrix(
                                result.getPose()).getData();
                        float[] modelViewProjectionButton = new float[16];

                        GLES20.glDepthFunc(GLES20.GL_LEQUAL);

                        GLES20.glEnable(GLES20.GL_BLEND);
                        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA,
                                GLES20.GL_ONE_MINUS_SRC_ALPHA);

                        // The inacuracy of the rendering process in some devices means
                        // that
                        // even if we use the "Less or Equal" version of the depth
                        // function
                        // it is likely that we will get ugly artifacts
                        // That is the translation in the Z direction is slightly
                        // different
                        // Another posibility would be to use a depth func "ALWAYS" but
                        // that is typically not a good idea
                        Matrix
                                .translateM(
                                        modelViewMatrixButton,
                                        0,
                                        0.0f,
                                        0.0f,
                                        targetPositiveDimensions[currentTarget].getData()[1] / 10.98f);
                        Matrix
                                .scaleM(
                                        modelViewMatrixButton,
                                        0,
                                        (targetPositiveDimensions[currentTarget].getData()[1] / 2.0f),
                                        (targetPositiveDimensions[currentTarget].getData()[1] / 2.0f),
                                        (targetPositiveDimensions[currentTarget].getData()[1] / 2.0f));
                        Matrix.multiplyMM(modelViewProjectionButton, 0,
                                projectionMatrix, 0, modelViewMatrixButton, 0);


                        GLES20.glUseProgram(keyframeShaderID);

                        GLES20.glVertexAttribPointer(keyframeVertexHandle, 3,
                                GLES20.GL_FLOAT, false, 0, quadVertices);
                        GLES20.glVertexAttribPointer(keyframeTexCoordHandle, 2,
                                GLES20.GL_FLOAT, false, 0, quadTexCoords);

                        GLES20.glEnableVertexAttribArray(keyframeVertexHandle);
                        GLES20.glEnableVertexAttribArray(keyframeTexCoordHandle);

                        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

                        // Depending on the status in which we are we choose the
                        // appropriate
                        // texture to display. Notice that unlike the video these are
                        // regular
                        // GL_TEXTURE_2D textures
                        switch (currentStatus[currentTarget]) {
                            case READY:
                                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                        mTextures.get(2).mTextureID[0]);
                                break;
                            case REACHED_END:
                                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                        mTextures.get(2).mTextureID[0]);
                                break;
                            case PAUSED:
                                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                        mTextures.get(2).mTextureID[0]);
                                break;
                            case NOT_READY:
                                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                        mTextures.get(3).mTextureID[0]);
                                break;
                            case ERROR:
                                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                        mTextures.get(4).mTextureID[0]);
                                break;
                            default:
                                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,
                                        mTextures.get(3).mTextureID[0]);
                                break;
                        }

                        GLES20.glUniformMatrix4fv(keyframeMVPMatrixHandle, 1, false,
                                modelViewProjectionButton, 0);
                        GLES20.glUniform1i(keyframeTexSampler2DHandle, 0);

                        // Render
                        GLES20.glDrawElements(GLES20.GL_TRIANGLES, NUM_QUAD_INDEX,
                                GLES20.GL_UNSIGNED_SHORT, quadIndices);

                        GLES20.glDisableVertexAttribArray(keyframeVertexHandle);
                        GLES20.glDisableVertexAttribArray(keyframeTexCoordHandle);

                        GLES20.glUseProgram(0);


                        // Finally we return the depth func to its original state
                        GLES20.glDepthFunc(GLES20.GL_LESS);
                        GLES20.glDisable(GLES20.GL_BLEND);

                }

            } else {
                // We reset the state of the animation so that
                // it triggers next time a vumark is detected
                blinkVumark(true);
                // We also reset the value of the current value of the vumark on card
                // so that we hide and show the mumark if we redetect the same vumark instance
                currentVumarkIdOnCard = null;
            }

        }

    }

    GLES20.glDisable(GLES20.GL_DEPTH_TEST);
    GLES20.glDisable(GLES20.GL_BLEND);

    Renderer.getInstance().end();

    }

    private float blinkVumark(boolean reset)
    {
        if (reset || t0 < 0.0f)
        {
            t0 = System.currentTimeMillis();
        }
        if (reset)
        {
            return 0.0f;
        }
        double time = System.currentTimeMillis();
        double delta = (time-t0);

        if (delta > 1000.0f)
        {
            return 1.0f;
        }

        if ((delta < 300.0f) || ((delta > 500.0f) && (delta < 800.0f)))
        {
            return 1.0f;
        }

        return 0.0f;
    }


    private float distanceSquared(Vec2F p1, Vec2F p2)
    {
        return (float) (Math.pow(p1.getData()[0] - p2.getData()[0], 2.0) +
                Math.pow(p1.getData()[1] - p2.getData()[1], 2.0));
    }

    private String instanceIdToType(InstanceId instanceId)
    {
        switch(instanceId.getDataType())
        {
            case InstanceId.ID_DATA_TYPE.STRING:
                return "String";

            case InstanceId.ID_DATA_TYPE.BYTES:
                return "Bytes";

            case InstanceId.ID_DATA_TYPE.NUMERIC:
                return "Numeric";
        }

        return "Unknown";
    }

    static final String hexTable = "0123456789abcdef";

    private String instanceIdToValue(InstanceId instanceId)
    {
        ByteBuffer instanceIdBuffer = instanceId.getBuffer();
        byte[] instanceIdBytes = new byte[instanceIdBuffer.remaining()];
        instanceIdBuffer.get(instanceIdBytes);

        String instanceIdStr = "";
        switch(instanceId.getDataType())
        {
            case InstanceId.ID_DATA_TYPE.STRING:
                instanceIdStr = new String(instanceIdBytes, Charset.forName("US-ASCII"));
                break;

            case InstanceId.ID_DATA_TYPE.BYTES:
                for (int i = instanceIdBytes.length - 1; i >= 0; i--)
                {
                    byte byteValue = instanceIdBytes[i];
                    instanceIdStr += hexTable.charAt((byteValue & 0xf0) >> 4);
                    instanceIdStr += hexTable.charAt(byteValue & 0x0f);
                }
                break;

            case InstanceId.ID_DATA_TYPE.NUMERIC:
                BigInteger instanceIdNumeric = instanceId.getNumericValue();
                Long instanceIdLong = instanceIdNumeric.longValue();
                instanceIdStr = Long.toString(instanceIdLong);
                break;

            default:
                return "Unknown";
        }

        return instanceIdStr;
    }

    private Bitmap getBitMapFromImage(Image image)
    {
        // we handle only RGB888 in this example
        if (image.getFormat() != PIXEL_FORMAT.RGBA8888) {
            // the default 'stock' image will be used for the instance image
            return null;
        }
        ByteBuffer pixels = image.getPixels();
        byte[] imgData = new byte[pixels.remaining()];
        pixels.get(imgData, 0, imgData.length);
        int width = image.getWidth();
        int height = image.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        int[] colors = new int[width * height];
        int r,g,b,a;
        for (int ci = 0; ci < colors.length; ci++)
        {
            r = (0xFF & imgData[4*ci]);
            g = (0xFF & imgData[4*ci+1]);
            b = (0xFF & imgData[4*ci+2]);
            a = (0xFF & imgData[4*ci+3]);
            colors[ci] = Color.argb(a, r, g, b);
        }

        bitmap.setPixels(colors, 0, width, 0, 0, width, height);
        return bitmap;
    }

    boolean isTapOnScreenInsideTarget(int target, float x, float y)
    {
        // Here we calculate that the touch event is inside the target
        Vec3F intersection;
        // Vec3F lineStart = new Vec3F();
        // Vec3F lineEnd = new Vec3F();

        DisplayMetrics metrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        intersection = SampleMath.getPointToPlaneIntersection(SampleMath
                        .Matrix44FInverse(tappingProjectionMatrix),
                modelViewMatrix[target], metrics.widthPixels, metrics.heightPixels,
                new Vec2F(x, y), new Vec3F(0, 0, 0), new Vec3F(0, 0, 1));

        // The target returns as pose the center of the trackable. The following
        // if-statement simply checks that the tap is within this range
        if ((intersection.getData()[0] >= -(targetPositiveDimensions[target]
                .getData()[0]))
                && (intersection.getData()[0] <= (targetPositiveDimensions[target]
                .getData()[0]))
                && (intersection.getData()[1] >= -(targetPositiveDimensions[target]
                .getData()[1]))
                && (intersection.getData()[1] <= (targetPositiveDimensions[target]
                .getData()[1])))
            return true;
        else
            return false;
    }


    void setVideoDimensions(int target, float videoWidth, float videoHeight,
                            float[] textureCoordMatrix)
    {
        // The quad originaly comes as a perfect square, however, the video
        // often has a different aspect ration such as 4:3 or 16:9,
        // To mitigate this we have two options:
        // 1) We can either scale the width (typically up)
        // 2) We can scale the height (typically down)
        // Which one to use is just a matter of preference. This example scales
        // the height down.
        // (see the render call in renderFrame)
        videoQuadAspectRatio[target] = videoHeight / videoWidth;

        float mtx[] = textureCoordMatrix;
        float tempUVMultRes[] = new float[2];

        if (target == videoPlayback.VIDEO1)
        {
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo1[0],
                    videoQuadTextureCoordsTransformedVideo1[1],
                    videoQuadTextureCoords[0], videoQuadTextureCoords[1], mtx);
            videoQuadTextureCoordsTransformedVideo1[0] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo1[1] = tempUVMultRes[1];
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo1[2],
                    videoQuadTextureCoordsTransformedVideo1[3],
                    videoQuadTextureCoords[2], videoQuadTextureCoords[3], mtx);
            videoQuadTextureCoordsTransformedVideo1[2] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo1[3] = tempUVMultRes[1];
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo1[4],
                    videoQuadTextureCoordsTransformedVideo1[5],
                    videoQuadTextureCoords[4], videoQuadTextureCoords[5], mtx);
            videoQuadTextureCoordsTransformedVideo1[4] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo1[5] = tempUVMultRes[1];
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo1[6],
                    videoQuadTextureCoordsTransformedVideo1[7],
                    videoQuadTextureCoords[6], videoQuadTextureCoords[7], mtx);
            videoQuadTextureCoordsTransformedVideo1[6] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo1[7] = tempUVMultRes[1];
        } else if (target == videoPlayback.VIDEO2)
        {
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo2[0],
                    videoQuadTextureCoordsTransformedVideo2[1],
                    videoQuadTextureCoords[0], videoQuadTextureCoords[1], mtx);
            videoQuadTextureCoordsTransformedVideo2[0] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo2[1] = tempUVMultRes[1];
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo2[2],
                    videoQuadTextureCoordsTransformedVideo2[3],
                    videoQuadTextureCoords[2], videoQuadTextureCoords[3], mtx);
            videoQuadTextureCoordsTransformedVideo2[2] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo2[3] = tempUVMultRes[1];
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo2[4],
                    videoQuadTextureCoordsTransformedVideo2[5],
                    videoQuadTextureCoords[4], videoQuadTextureCoords[5], mtx);
            videoQuadTextureCoordsTransformedVideo2[4] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo2[5] = tempUVMultRes[1];
            tempUVMultRes = uvMultMat4f(
                    videoQuadTextureCoordsTransformedVideo2[6],
                    videoQuadTextureCoordsTransformedVideo2[7],
                    videoQuadTextureCoords[6], videoQuadTextureCoords[7], mtx);
            videoQuadTextureCoordsTransformedVideo2[6] = tempUVMultRes[0];
            videoQuadTextureCoordsTransformedVideo2[7] = tempUVMultRes[1];
        }

        // textureCoordMatrix = mtx;
    }


    // Multiply the UV coordinates by the given transformation matrix
    float[] uvMultMat4f(float transformedU, float transformedV, float u,
                        float v, float[] pMat)
    {
        float x = pMat[0] * u + pMat[4] * v /* + pMat[ 8]*0.f */+ pMat[12]
                * 1.f;
        float y = pMat[1] * u + pMat[5] * v /* + pMat[ 9]*0.f */+ pMat[13]
                * 1.f;
        // float z = pMat[2]*u + pMat[6]*v + pMat[10]*0.f + pMat[14]*1.f; // We
        // dont need z and w so we comment them out
        // float w = pMat[3]*u + pMat[7]*v + pMat[11]*0.f + pMat[15]*1.f;

        float result[] = new float[2];
        // transformedU = x;
        // transformedV = y;
        result[0] = x;
        result[1] = y;
        return result;
    }


    void setStatus(int target, int value)
    {
        // Transform the value passed from java to our own values
        switch (value)
        {
            case 0:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.REACHED_END;
                break;
            case 1:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.PAUSED;
                break;
            case 2:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.STOPPED;
                break;
            case 3:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.PLAYING;
                break;
            case 4:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.READY;
                break;
            case 5:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.NOT_READY;
                break;
            case 6:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.ERROR;
                break;
            default:
                currentStatus[target] = videoPlayerHelper.MEDIA_STATE.NOT_READY;
                break;
        }
    }


    boolean isTracking(int target)
    {
        return isTracking[target];
    }


    public void setTextures(Vector<Texture> textures)
    {
        mTextures = textures;
    }

}
